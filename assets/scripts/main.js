var html_popup;
$('document').ready(function(){
	$(".js__heapbox").select2({
		 minimumResultsForSearch: Infinity
	});
	
	$(".js__heapbox").on("select2:open",function(){
		$(".select2-results").mCustomScrollbar({
			scrollInertia: 200
		});
	});
	
	$(".js__dropdown").on("click",function(){
		$(this).toggleClass("js__active");
		$($(this).data("target")).stop().slideToggle(400);
	});
	
	$("#wrapper").on("click",function(event){
		if ($(".js__dropdown.js__active").length){
			var selector = $(event.target);
			if (selector.hasClass("js__dropdown") || selector.parents(".js__dropdown").length || selector.hasClass("js__dropdown_target") || selector.parents(".js__dropdown_target").length){
				//do nothing
			}else{
				$(".js__dropdown.js__active").removeClass("js__active");
				$(".js__dropdown_target").stop().slideUp(400);
			}
		}
	});
	
	$(".js__main_slider").flexslider({
		directionNav: false,
		animation: "slide",
		minItems: 1,
		maxItems: 1,
	});

	film_slider();
	
	$(".js__tab").on("click",function(){
		$(".js__tab.js__active,.js__tab_content.js__active").removeClass("js__active")
		$(this).addClass("js__active");
		$($(this).data("target")).addClass("js__active");
	});
	
	$(".js__promotion_tab").on("click",function(){
		$(".js__promotion_tab.js__active,.js__promotion_tab_content.js__active").removeClass("js__active")
		$(this).addClass("js__active");
		$($(this).data("target")).addClass("js__active");
	});
	
	$("#toTop").on("click",function(){
		$(window).scrollTo("0px",400)
	});
	
	$(".js__popup").on("click",function(event){
		event.preventDefault();
		if (html_popup){
			html_popup.appendTo("body");
			html_popup = null;
		}
		$($(this).data("target")).addClass("js__active");
		$("body").addClass("js__popup_active");
	});
	
	$(".js__popup_close").on("click",function(event){
		event.preventDefault();
		$(this).parents(".js__popup_modal").removeClass("js__active");
		$("body").removeClass("js__popup_active");
		html_popup = $(this).parents(".js__popup_modal").detach();
	});
});

$(window).on("resize",function(){
	film_slider();
});

$(window).on("scroll",function(){
	if ($(window).scrollTop() > 600){
		$("#toTop").show();
	}else{
		$("#toTop").hide();
	}
});

$(window).load(function() {
    $('.js__flexslider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		pauseOnHover: true,
		controlNav: false, 
		directionNav: false,
		direction: "vertical"
	});
	
	$(".js__flexslider_promotion").flexslider({
		animation: "slide",
		controlNav: true, 
		directionNav: false,
		itemWidth: 570,
		itemMargin: 30,
		move: 1
	});
	
	
	$(".js__flexslider_promotion_film_detail").flexslider({
		animation: "slide",
		controlNav: false, 
		directionNav: true,
		itemWidth: 680,
		itemMargin: 30,
		move: 1,
		item: 1,
	});
	
	$(".js__film_slider").each(function(){
		$(this).flexslider({
			animation: "slide",
			controlNav: false, 
			directionNav: true,
			itemWidth: 269,
			itemMargin: 30,
			move: 1
		});
	})
	$(".js__list_film_slider").each(function(){
		$(this).flexslider({
			animation: "slide",
			controlNav: true, 
			directionNav: false,
			itemWidth: 269,
			itemMargin: 30,
			move: 1
		});
	})
});

$(window).bind("load",function(){
	if($(".js__isotope").length){
		$(".js__isotope").each(function(){
			var selector = $(this);
			setTimeout(function(){
				selector.isotope({
					itemSelector: ".js__isotope_item"
				})
			},100);
		});
	}
});
 
function film_slider(){
	if ($(".js__film").length){
		var window_width = $(window).width() - 90;
		$(".js__film").each(function(){
			var number_view = Math.floor(window_width / 299);
			if (number_view == 0) number_view = 1;
			if (number_view < $(this).find("li").length){
				$(this).css({
					"width" : number_view * 299 - 30
				});
			}else{
				$(this).css({
					"width" : $(this).find("li").length * 299 - 30
				});
			}
		});
	}
}